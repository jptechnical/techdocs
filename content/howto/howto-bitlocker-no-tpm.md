---
date: 2022-10-14
title: How-to Enable BitLocker Without a TPM
slug: howto-bitlocker-no-tpm
author: Jesse Perry
publish: true
draft: false
type: blog
tags:
- encryption
- windows
---
It is sometimes necessary to enable BitLocker on devices that do not contain a TPM module. This is NOT ideal, but it is necessary in some rare circumstances. 

This is how you can allow the use of pin#s and USB-Keys as a way to unlock a BitLocker drive.

1. Enable the Group Policy object `Require additional authentication at startup`. 
    - Open `gpedit.msc` 
    - Browse to `Computer Configuration > Administrative Templates >  Windows Components > BitLocker Drive Encryption > Operating System Drives`. 
    - Enable the `Require additional authentication at startup` object

1. Add the following settings
    - Allow Bitlocker without a compatible TPM - Yes.
    - Configure TPM startup - Allow TPM.
    - Configure TPM startup PIN - Allow startup PIN with TPM.
    - Configure TPM startup key - Allow startup key with TPM.
    - Configure TPM startup key and PIN - Allow startup key and PIN with TPM.

1. Check for a TPM module in PowerShell with `get-tpm`.

    ```powershell
    PS C:\Windows\system32> Get-Tpm
      
      TpmPresent                : False
      TpmReady                  : False
      TpmEnabled                : False
      TpmActivated              : False
      TpmOwned                  : False
      RestartPending            : False
      ManufacturerId            : 0
      ManufacturerIdTxt         :
      ManufacturerVersion       :
      ManufacturerVersionFull20 :
      ManagedAuthLevel          : Full
      OwnerAuth                 :
      OwnerClearDisabled        : True
      AutoProvisioning          : NotDefined
      LockedOut                 : False
      LockoutHealTime           :
      LockoutCount              :
      LockoutMax                :
      SelfTest                  :
    ```
  
1. Check for drive encryption with PowerShell using the command `Get-BitlockerVolume`.

    ```powershell
    PS C:\Windows\system32> Get-BitlockerVolume

        ComputerName: TestComputer
        VolumeType      Mount CapacityGB VolumeStatus      Encryption   KeyProtector   AutoUnlock Protection
                        Point                              Percentage                  Enabled    Status
        ----------      ----- ---------- ------------      ----------   ------------   ---------- ----------
        OperatingSystem C:        346.36 FullyDecrypted    0            {}                        Off
        Data            D:          3.82 FullyDecrypted    0            {}                        Off
        ```

1. Encrypt the drive with PowerShell with this long command, `enable-BitLocker -MountPoint "C:" -UsedSpaceOnly -EncryptionMethod Aes256 -StartupKeyPath "D:" -StartupKeyProtector`.

    ```powershell
    PS C:\Windows\system32> enable-BitLocker -MountPoint "C:" -UsedSpaceOnly -EncryptionMethod Aes256 -StartupKeyPath "D:" -StartupKeyProtector

       WARNING: ACTIONS REQUIRED:

        1.Insert a USB flash drive with an external key file into the computer.
        2. Restart the computer to run a hardware test.
            (Type: get-help Restart-Computer for command line instructions.)
        
           ComputerName: TestComputer
        
        VolumeType      Mount CapacityGB VolumeStatus           Encryption KeyProtector              AutoUnlock Protection
                        Point                                   Percentage                           Enabled    Status
        ----------      ----- ---------- ------------           ---------- ------------              ---------- ----------
        OperatingSystem C:        346.36 FullyDecrypted         0          {ExternalKey}                        Off 
    ```

1. Finally, make sure you grab the Recovery Key if it is not being escrowed somewhere for you, like with JumpCloud. Here is the command, notice that it identifies both the external key files (on the D: drive), as well as the recovery password. The command is `(Get-BitLockerVolume -MountPoint C).KeyProtector`.

    ```powershell
    PS C:\Windows\system32> (Get-BitLockerVolume -MountPoint C).KeyProtector
        
        KeyProtectorId      : {12345678-502A-4085-B6F6-1234567890}
        AutoUnlockProtector :
        KeyProtectorType    : ExternalKey
        KeyFileName         : 12345678-502A-4085-B6F6-1234567890.BEK
        RecoveryPassword    :
        KeyCertificateType  :
        Thumbprint          :
        
        KeyProtectorId      : {12345678-38D4-4228-8B0B-1234567890}
        AutoUnlockProtector :
        KeyProtectorType    : RecoveryPassword
        KeyFileName         :
        RecoveryPassword    : 123456-123456-123456-123456-123456-123456-123456-123456
        KeyCertificateType  :
        Thumbprint          :
    ```

1. If for some bizarre reason this does not produce a recovery password in the list, here is the command to create one. Be sure to take note of it, or escrow it in JumpCloud. The command is `Add-BitlockerkeyProtector C: -RecoveryPasswordProtector`.

    ```powershell
    PS C:\Windows\system32> Add-BitlockerkeyProtector C: -RecoveryPasswordProtector
        WARNING: ACTIONS REQUIRED:
        
        1. Save this numerical recovery password in a secure location away from your computer:
        
        123456-123456-123456-123456-123456-123456-123456-123456
        
        To prevent data loss, save this password immediately. This password helps ensure that you can unlock the encrypted
        volume.
        
           ComputerName: TestComputer
        
        VolumeType      Mount CapacityGB VolumeStatus           Encryption KeyProtector              AutoUnlock Protection
                        Point                                   Percentage                           Enabled    Status
        ----------      ----- ---------- ------------           ---------- ------------              ---------- ----------
        OperatingSystem C:        346.36 FullyEncrypted         100        {ExternalKey, External...            On
    ```

following  this article: [Tutorial Powershell - Encrypt the disk using Bitlocker and USB key](https://techexpert.tips/powershell/powershell-encrypt-disk-bitlocker-usb-key/)