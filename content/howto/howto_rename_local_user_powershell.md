---
date: 2022-07-21
title: How-to Rename Local User Powershell
slug: howto_rename_local_user_powershell
author: Jesse Perry
publish: false
draft: true
type: blog
tags:
-
---
To rename a user in PowerShell, you can use the `Rename-LocalUser` cmdlet.
Here's how:

Open PowerShell as an administrator.

Type the following command, replacing "OldUserName" with the current name of
the user and "NewUserName" with the desired new name:

```powershell
Rename-LocalUser -Name "OldUserName" -NewName "NewUserName"
```

If the operation is successful, you should see a confirmation message in the PowerShell console.

Note that you will need to be logged in as an administrator or have
administrative privileges to use the `Rename-LocalUser` cmdlet. Additionally,
the user account should not be currently in use, so you may need to log out of
the account first before renaming it.
