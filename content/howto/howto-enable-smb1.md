---
date: 2022-11-07
title: How-to Enable SMB1 Protocol on Windows 
slug: howto-enable-smb1-cmd
author: Jesse Perry
publish: true
draft: false
type: blog
tags:
- windows
- danger
---

> [!danger] This is a known security risk. This should not be used in production!

While it is NOT advised, it is necessary to enable SMB1 support to let Windows 10 access old SMBv1 shares. This is a security risk, and is advised against, but if you need to, here is the command. 

```cmd
DISM /Online /Enable-Feature /All /FeatureName:SMB1Protocol
```

Here is the output. 

```cmd

Deployment Image Servicing and Management tool
Version: 10.0.19041.844

Image Version: 10.0.19045.2130

Enabling feature(s)
[==========================100.0%==========================]
The operation completed successfully.
Restart Windows to complete this operation.
Do you want to restart the computer now? (Y/N)
```

> [!Note:] It is necessary to restart the computer after enabling this feature.
