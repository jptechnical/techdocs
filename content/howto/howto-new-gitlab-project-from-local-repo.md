---
date: "2023-02-17"
title: "How-to Create a New Gitlab Project from a Local Repository"
slug: gitlab-project-from-local-repo
publish: true
draft: false
type: doc
tags:
  - git
  - terminal
---
GitLab is a popular web-based Git repository manager; I prefer
it to the other one owned by the evil empire. If you have a Git project on your
local machine and want to upload it to GitLab for collaboration, this guide
will walk you through the steps.

> [TL;DR]
> For GitLab users who simply want a quick overview of the commands to run in
> the existing project to push to GitLab, here they are. These commands assume
> a GitLab repository named 'example-website', and a user account named
> jptechnical.

```bash
  # Initilize the local repository
git init
  # Add everying, because I like to live dangerously
git add .
  # Add initial commit
git commit -m "Push existing project to GitLab"
  # Add remote origin
  # if you haven't setup your ssh key, use https instead with #   https://gitlab.com/jptechnical/example-website.git
git remote add source git@gitlab.com:jptechnical/example-website.git
  # Push to gitlab
git push -u -f source main
```

## Prerequisites 

Before you begin, make sure you have the following:

A GitLab account with a project already created Git installed on your local
machine 

### Step 1: Create a New Git Repository

Open the terminal and navigate to
the directory of the project you want to upload to GitLab. Once you are in the
project directory, run the following command:

```bash
git initialize
```

This will initialize a new Git repository in your project directory.

### Step 2: Add Your Files

Next, add all of your project files to the Git
repository by running the following command:

```bash
git add .
```

This will add all of your files to the staging area.

### Step 3: Commit Your Changes

Commit your changes by running the following command:

```bash
git commit -m "Initial commit"
```

This will commit all the changes you have made to your Git repository.

### Step 4: Add Your GitLab Remote

Now that you have a Git repository on your local
machine, you need to add your GitLab repository as a remote. To do this, run
the following command:

```bash
git remote add origin <GitLab repository URL>
```

Replace <GitLab repository URL> with the URL of your GitLab repository.

### Step 5: Push Your Changes to GitLab

Finally, push your changes to GitLab by running the following command:

```bash
git push -u origin master
```

This will push your changes to the master branch of your GitLab repository. You
may be prompted to enter your GitLab username and password.

### Conclusion

That's it! You have successfully uploaded a local Git project to
GitLab. From here, you can use GitLab's powerful features to manage your code,
collaborate with others, and streamline your development workflow.
