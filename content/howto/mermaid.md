---
date: "2022-04-23"
title: "How-to use Mermaid Diagrams"
publish: true
draft: false
type: doc
---
Mermaid is a diagramming engine for Markdown. A live editor and tutorial can be found at [mermaid.live](https://mermaid.live/) and is very helpful for prototyping diagrams.

## Basic syntax
Below is an example of what the markdown looks like for a simple flowchart. 

```mermaid
graph LR
A["☁️" - GCI] --> B(Unifi USG Pro)
B --> C{Let me think}
C -->|One| D[Laptop]
C -->|Two| E[iPhone]
C -->|Three| F[fa:fa-cloud Car]
```

And here is the code behind this flowchart.

````markdown
```mermaid
graph TD
A["☁️" - GCI] --> B(Unifi USG Pro)
B --> C{Let me think}
C -->|One| D[Laptop]
C -->|Two| E[iPhone]
C -->|Three| F[fa:fa-cloud Car]
```
````

Long lines can be broken as long as you wrap it all in double quotes. Also, HTML can be used to include line breaks or other formatting.

```mermaid
graph LR
A["☁️" - GCI] --> B("Unifi USG Pro 
  here is some long line of text
  and this is being broken in the 
  code but not on the
  resulting graph")
B --> C{Let <br>me <br>think }
C -->|One| D[<em>Laptop</em>]
C -->|Two| E[<strong>iPhone</strong>]
C -->|Three| F[fa:fa-cloud Car]
```

And here is the corresponding code.

````markdown
```mermaid
graph LR
A["☁️" - GCI] --> B("Unifi USG Pro 
  here is some long line of text
  and this is being broken in the 
  code but not on the
  resulting graph")
B --> C{Let <br>me <br>think }
C -->|One| D[<em>Laptop</em>]
C -->|Two| E[<strong>iPhone</strong>]
C -->|Three| F[fa:fa-cloud Car]
```
````

The element names are also completely arbitrary, the examples above use `A`, `B`, etc, but in the network diagram we name the elements to they are clearly understood.

## Example of a Network Diagram in Mermaid

Here is a diagram of a network in [flowchart] form going from the ISP to the local network devices. Notice that the relationships are easily identified even when viewed as code. In this case, we are using the `graph TD` to make a top-down diagram instead of `graph LR`, which creates a left-to-right diagram.

````markdown
```mermaid
graph TD
ISP["☁" GCI] --> ROUTER(Unifi USG Pro)
ROUTER --> SWITCH{Unifi 16P Switch}
SWITCH -->|PortOne| LAPTOP[Laptop]
SWITCH -->|PortTwo| SERVER[Server]
SWITCH -->|PortThree| AP{Unifi AP-ACPro}
AP -->|Wireless| CELL1[Cellphone]
AP -->|Wireless| CELL2[Cellphone]
AP -->|Wireless| CELL3[Cellphone]
AP -->|Wireless| CELL4[Cellphone]
```
````

Here is the resulting diagram from the code above.

```mermaid
graph TD
ISP["☁" GCI] --> ROUTER(Unifi USG Pro)
ROUTER --> SWITCH{Unifi 16P Switch}
SWITCH -->|PortOne| LAPTOP[Laptop]
SWITCH -->|PortTwo| SERVER[Server]
SWITCH -->|PortThree| AP{Unifi AP-ACPro}
AP -->|Wireless| CELL1[Cellphone]
AP -->|Wireless| CELL2[Cellphone]
AP -->|Wireless| CELL3[Cellphone]
AP -->|Wireless| CELL4[Cellphone]
```
