---
date: "2022-05-27"
title: "How-to create a VM using a Ubuntu Cloud image on ProxMox"
slug: howto-create-vm-cloud-img-proxmox
publish: true
draft: false
type: doc
tags:
  - proxmox
  - howto
  - linux
---

## Create the VM

We want some special stuff here, so it is easier to do from the terminal. Here are the commands thus far, these are run as the `root` user.

```bash
# Download the ubuntu image
curl -O https://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64.img
# Create a vm, I use a high ID number to make it clear
qm create 8000 --memory 2048 --name ubuntu-cloud --net0 virtio,bridge=vmbr0
# Import the disk image to storage. 'lvm' is the name of one of my storages
qm importdisk 8000 ubuntu-20.04-server-cloudimg-amd64.img lvm
# Add a scsi controller and attach the disk created earlier
qm set 8000 --scsihw virtio-scsi-pci --scsi0 lvm:vm-8000-disk-0
# Add a cdrom for the cloudinit 
qm set 8000 --ide2 lvm:cloudinit
# Set boot device
qm set 8000 --boot c --bootdisk scsi0
# Add a serial port
qm set 8000 --serial0 socket --vga serial0
```


## Configure the `cloudinit` settings

Go back to the ProxMox controller and see your new VM, and go to the Cloud-Init tab. From here we set the defaults for the new VMs. I will go through the settings one by one. 

- User: `ubuntu`
- Password: `ubuntu`
  - The first user in the system has `sudo` rights without a password. So we can leave this blank. The only issue here is you can't login to the terminal without a password, only via SSH, so you need to find the IP address.
- DNS domain: set to the internal domain
- DNS servers: `x.x.x.x 8.8.8.8`, this is the internal DNS address and a fallback.
- SSH public key: `jptechnical internal ssh key` from the [[Secrets]] in the `_Internal` folder.


## Configure the rest of the VM

I set my template to have 4GB of ram and 4 CPU cores. That really is the only change I made. 


## Turn this VM into a Template

This is a **destructive process**, you can't change it once it's a template. But, you could clone it, make your changes and make the clone into a template. 

Whatever you do, **DON'T start the VM** before you make it a template, otherwise the disk image will get it's cloudinit stuff assigned and you are going to have to do it all over again, otherwise your devices won't be unique when you clone them.
