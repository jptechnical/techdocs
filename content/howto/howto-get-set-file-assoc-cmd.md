---
date: 2022-10-12
title: How-to Get & Set File Associations from the Command Prompt
slug: howto-get-set-file-assoc-cmd
author: Jesse Perry
publish: true
draft: false
type: blog
tags:
- windows
- cmd
---

The `assoc` command will list filetype associations. When the command is run by
itself it spits out all the associations as seen in the following example. 

```cmd
C:\Windows\system32>assoc
.386=vxdfile
.3g2=WMP11.AssocFile.3G2
.3gp=WMP11.AssocFile.3GP
.3gp2=WMP11.AssocFile.3G2
.3gpp=WMP11.AssocFile.3GP
.AAC=WMP11.AssocFile.ADTS
.acrobatsecuritysettings=AcroExch.acrobatsecuritysettings
...
.xsl=xslfile
.zfsendtotarget=CLSID\{888DCA60-FC0A-11CF-8F0F-00C04FD7D062}
.zip=CompressedFolder

```

You can filter the results by using the `findstr` command to search for
specific strings. In our case we will search for the`pdf` filetype with the
`assoc | findstr pdf` command as seen below.

```cmd
C:\Windows\system32>assoc | findstr pdf
.pdf=AcroExch.Document
.pdfxml=AcroExch.pdfxml
.vxd=vxdfile
.xfdf=AcroExch.XFDFDoc
```

We can then set the filetype association by specifing it on the command line.
For example, if we ran ` assoc .pdf=FoxitReader.Document`, then it would set
the filetype of `.pdf` to the  `FoxitReader.Document` application. The example
below shows the output.

```cmd
C:\Windows\system32>assoc .pdf=FoxitReader.Document
.pdf=FoxitReader.Document
```

Finally, let's confirm that the change has been made by re-running the previous
command.

```cmd
C:\Windows\system32>assoc | findstr pdf
.pdf=FoxitReader.Document
.pdfxml=AcroExch.pdfxml
```
