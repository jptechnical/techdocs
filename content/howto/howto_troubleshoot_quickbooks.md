---
date: 2022-10-06
title: How-to Troubleshoot Quickbooks
slug: howto_troubleshoot_quickbooks
author: Jesse Perry
publish: true
draft: false
type: blog
tags:
- quickbooks
- troubleshoot
---
Quickbooks can be pretty touchy, sometimes it just throws an error that the file is unavailable or something related to sharing it. The simplest is to use the **Quickbooks Database Server Manager** to scan for files and reset the sharing. 

The screenshot below shows what this tool looks like and the steps to take.

1. Click the Start button
1. Type `Quickbooks Database Server Manager`
1. Open the `Quickbooks Database Server Manager` application
1. Browse for the folder containing the Company Files
1. Once selected, confirm the folder path here.
1. Click the `Start Scan` button.
1. The scan will go through several steps, it will indicate when it is complete and successful. Note that the example below warns that an Antivirus might interfere, this is helpful to check if the issue persists.
1. Finally, close the `Quickbooks Database Server Manager` window and ask the end-user to try again.

![](howto_troubleshoot_quickbooks.png)