---
date: 2022-11-21
title: How-to Bypass the Execution Policy for a Powershell Script
slug: howto-bypass-execution-policy-powershell
author: Jesse Perry
publish: false
draft: true
type: blog
tags:
- 
---

Here is a quick trick for bypassing the execution policy to run a Powershell script. 

## Read Script from a File and Pipe to PowerShell Standard In

Use the Windows "type" command or PowerShell "Get-Content" command to read your script from the disk and pipe it into PowerShell standard input. This technique does not result in a configuration change, but does require writing your script to disk. However, you could read it from a network share if you’re trying to avoid writing to the disk.

```powershell
Get-Content .runme.ps1 | PowerShell.exe -noprofile -
```
