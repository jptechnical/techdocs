---
date: "2023-02-17"
title: "How-To Guides"
publish: true
draft: false
type: chapter
---
We follow the [Divio standard](https://documentation.divio.com/), a set of guidelines for creating high-quality tutorials and documentation for web development projects. Here are some of the key principles of tutorials in the Divio standard:

The principles of how-tos in the Divio Standard are as follows:

Focus on the task at hand: A how-to should be focused on a specific task or problem that the user is trying to solve. It should not contain extraneous information that is not relevant to the task.

Use simple language: The language used in the how-to should be simple and easy to understand. Avoid using technical jargon or complex terminology that may be unfamiliar to the user.

Use a step-by-step format: The how-to should be presented in a step-by-step format, with each step clearly outlined and numbered. This helps to ensure that the user can easily follow the instructions and complete the task.

Provide examples: Examples can be helpful in demonstrating how to complete a task. If possible, provide examples that are relevant to the user's specific situation.

Use screenshots or other visual aids: Screenshots or other visual aids can be used to help illustrate the steps in the how-to. These can be particularly helpful for users who are visual learners.

Test the how-to: Before publishing the how-to, it is important to test it to ensure that the instructions are accurate and that the task can be completed successfully.

By following these principles, a how-to created according to the Divio Standard should be clear, concise, and easy to understand, helping users to complete tasks related to web development more efficiently and effectively., our customers, and the wider MSP community. By creating a centralized repository of technical documentation, we can ensure that our knowledge is up-to-date and easily accessible. We look forward to expanding TechDocs over time and sharing our expertise with others in the industry.
